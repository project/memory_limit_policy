<?php

namespace Drupal\Tests\memory_limit_policy\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests memory_limit_policy module.
 *
 * @group memory_limit_policy
 */
class MemoryLimitPolicyTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'memory_limit_policy',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('memory_limit_policy.settings')
      ->set('header', FALSE)
      ->save();
  }

  /**
   * Validate header config adds X-Memory-Limit-Memory-* headers.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testDebugHeaders() {
    // Assert the response does not get the headers by default.
    $this->drupalGet('');
    $this->assertSession()->responseHeaderDoesNotExist('X-Memory-Limit-Memory');
    $this->assertSession()->responseHeaderDoesNotExist('X-Memory-Limit-Override');
    $this->assertSession()->responseHeaderDoesNotExist('X-Memory-Limit-Policy-Name');

    $this->config('memory_limit_policy.settings')
      ->set('header', TRUE)
      ->save();

    // Switching the config does not invalidate the page cache so we do it.
    drupal_flush_all_caches();

    // Assert the response get the headers with default values.
    $this->drupalGet('');

    // @TODO: Find a way to validate the memory in the header. ini_get() may
    // vary in btw the test and page load process.
    $this->assertSession()->responseHeaderExists('X-Memory-Limit-Memory');
    $this->assertSession()->responseHeaderEquals('X-Memory-Limit-Override', '0');
    $this->assertSession()->responseHeaderDoesNotExist('X-Memory-Limit-Policy-Name');
  }

}
