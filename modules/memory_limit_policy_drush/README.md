CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Memory limit policy Drush is a module to enable constraints for drush
commands. This will allow user to override the default php memory_limit for
drush commands.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

1. Visit /admin/config/performance/memory-limit-policy/list to configure
 policies.
1. Click on `Add Policy` button
1. Add the following details
    1. Policy Name: The name of the Policy.
    1. Memory: The limit to set for the Drush command
    1. Enabled: Mark this checkbox.
    Click on `Next` Button to add Constraint.
1. Select the `Drush` from the dropdown and click on `Configure Constraint Settings`
1. In the modal form add the drush command one command name per line.
1. Click on `Save` and then `Finish` button.


MAINTAINERS
-----------

 * rahul.shinde - https://www.drupal.org/u/rahulshinde
