<?php

namespace Drupal\memory_limit_policy_drush\Plugin\MemoryLimitConstraint;

use Drupal\Core\Form\FormStateInterface;
use Drupal\memory_limit_policy\MemoryLimitConstraintBase;

/**
 * Configure the memory limit based on path.
 *
 * @MemoryLimitConstraint(
 *   id = "drush",
 *   title = @Translation("Drush"),
 *   description = @Translation("Provide a list of drush commands where the memory limit must be overridden.")
 * )
 */
class Drush extends MemoryLimitConstraintBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['drush_commands'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Drush commands'),
      '#description' => $this->t('Enter one drush command per line.'),
      '#default_value' => $this->getConfiguration()['drush_commands'] ?? '',
    ];

    $form['drush_warning'] = [
      '#type' => 'markup',
      '#markup' => $this->t("It is known that memory limit policy won't apply for some core Drush commands (typically commands defined in <a href='https://github.com/drush-ops/drush/tree/11.x/src/Commands'>src/Commands</a>) due to the order files are loaded. See <a href='https://www.drupal.org/project/memory_limit_policy/issues/3276442'>#3276442</a> for more info."),
      '#prefix' => '<span class="form-item__description color-error">',
      '#suffix' => '</span>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['drush_commands'] = $form_state->getValue('drush_commands');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $drush_commands = explode(PHP_EOL, $this->configuration['drush_commands']);
    array_walk($drush_commands, function (&$drush_command) {
      $drush_command = rtrim(trim($drush_command, "\r"), '/');
    });
    $drush_commands = implode(', ', $drush_commands);

    return $this->t('Drush commands: @drush_commands', ['@drush_commands' => $drush_commands]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    // For Drush constraints we are not evaluating anything here. So this
    // will always return FALSE (TRUE if negated) for clients other than CLI.
    return $this->isNegated();
  }

}
