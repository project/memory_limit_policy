<?php

namespace Drupal\memory_limit_policy_http_method\Plugin\MemoryLimitConstraint;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\memory_limit_policy\MemoryLimitConstraintBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Configure the memory limit based on HTTP method.
 *
 * @MemoryLimitConstraint(
 *   id = "http_method",
 *   title = @Translation("HTTP method"),
 *   description = @Translation("Provide a list of HTTP methods for which the memory limit must be overridden.")
 * )
 */
class HttpMethod extends MemoryLimitConstraintBase implements ContainerFactoryPluginInterface {


  /**
   * The request stack.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs constraint plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $requestStack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['methods'] = [
      '#type' => 'checkboxes',
      '#options' => [
        'get' => 'GET',
        'head' => 'HEAD',
        'post' => 'POST',
        'put' => 'PUT',
        'delete' => 'DELETE',
        'connect' => 'CONNECT',
        'options' => 'OPTIONS',
        'trace' => 'TRACE',
        'patch' => 'PATCH',
      ],
      '#title' => $this->t('HTTP methods'),
      '#description' => $this->t('Select the HTTP methods for which the policy must apply.'),
      '#default_value' => $this->getConfiguration()['methods'] ?? '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['methods'] = array_keys(array_filter($form_state->getValue('methods')));
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return $this->t('Methods: @methods', ['@methods' => strtoupper(implode(', ', $this->configuration['methods']))]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    return in_array(strtolower($this->requestStack->getCurrentRequest()->getMethod()), $this->configuration['methods']);
  }

}
