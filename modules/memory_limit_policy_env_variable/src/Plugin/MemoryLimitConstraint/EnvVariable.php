<?php

namespace Drupal\memory_limit_policy_env_variable\Plugin\MemoryLimitConstraint;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\memory_limit_policy\MemoryLimitConstraintBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the memory limit based on an environment variable.
 *
 * @MemoryLimitConstraint(
 *   id = "env_variable",
 *   title = @Translation("Environment variable"),
 *   description = @Translation("Provide an environmnet variable and associated value for which the memory limit must be overridden.")
 * )
 */
class EnvVariable extends MemoryLimitConstraintBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['name'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Name'),
      '#description' => $this->t('The name of the variable to check.'),
      '#default_value' => $this->getConfiguration()['name'] ?? '',
    ];

    $form['values'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Values'),
      '#description' => $this->t('Enter one value per line. Value can be a regular expression pattern, without delimiters. Leave empty to match any value.'),
      '#default_value' => implode(PHP_EOL, $this->getConfiguration()['values'] ?? []) ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['name'] = trim($form_state->getValue('name'));

    // Transform textarea into list of values.
    $this->configuration['values'] = array_filter(
      preg_split("/\r?\n/", $form_state->getValue('values')),
      function ($value) {
        return trim($value);
      }
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    if (empty($this->getConfiguration()['values'])) {
      return $this->t('Environment variable @name', ['@name' => $this->getConfiguration()['name']]);
    }
    else {
      return $this->t('Environment variable @name matches @values', [
        '@name' => $this->getConfiguration()['name'],
        '@values' => implode(', ', $this->getConfiguration()['values']),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $name = $this->getConfiguration()['name'];

    if (!isset($_ENV[$name])) {
      return FALSE;
    }

    $values = $this->getConfiguration()['values'] ?? [];

    if (empty($values)) {
      return TRUE;
    }

    if (in_array($_ENV[$name], $values)) {
      return TRUE;
    }

    foreach (array_map(fn ($value) => sprintf('{%s}i', $value), $values) as $pattern) {
      if (preg_match($pattern, $_ENV[$name])) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
