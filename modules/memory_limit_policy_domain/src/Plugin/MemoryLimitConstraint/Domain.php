<?php

namespace Drupal\memory_limit_policy_domain\Plugin\MemoryLimitConstraint;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\memory_limit_policy\MemoryLimitConstraintBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Configure the memory limit based on domain.
 *
 * @MemoryLimitConstraint(
 *   id = "domain",
 *   title = @Translation("Domain"),
 *   description = @Translation("Provide a list of domains where the memory limit must be overridden.")
 * )
 */
class Domain extends MemoryLimitConstraintBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs constraint plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['domains'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Domains'),
      '#description' => $this->t('Enter one domain per line. Use plain host or a regular expression pattern, without delimiters like the <a href="https://www.drupal.org/docs/getting-started/installing-drupal/trusted-host-settings" target="_blank">Trusted Host settings</a>.'),
      '#default_value' => implode(PHP_EOL, $this->getConfiguration()['domains'] ?? []) ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    // Transform textarea into list of values.
    $this->configuration['domains'] = array_filter(
      preg_split("/\r?\n/", $form_state->getValue('domains')),
      function ($domain) {
        return trim($domain);
      }
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return $this->t('Domains: @domains', ['@domains' => implode(', ', $this->getConfiguration()['domains'])]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $configured_domain_list = $this->getConfiguration()['domains'] ?? [];

    $request = $this->requestStack->getCurrentRequest();

    // Do a plain text comparison first.
    if (in_array($request->getHost(), $configured_domain_list)) {
      return TRUE;
    }

    // Do a regular expression comparison.
    foreach (array_map(fn ($domain) => sprintf('{%s}i', $domain), $configured_domain_list) as $pattern) {
      if (preg_match($pattern, $request->getHost())) {
        return TRUE;
      }
    }

    return parent::evaluate();
  }

}
