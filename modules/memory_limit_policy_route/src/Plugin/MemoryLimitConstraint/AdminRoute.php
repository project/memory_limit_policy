<?php

namespace Drupal\memory_limit_policy_route\Plugin\MemoryLimitConstraint;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\memory_limit_policy\MemoryLimitConstraintBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the memory limit based on admin route.
 *
 * @MemoryLimitConstraint(
 *   id = "admin_route",
 *   title = @Translation("Admin route"),
 *   description = @Translation("Allows overriding memory limits for admin routes.")
 * )
 */
class AdminRoute extends MemoryLimitConstraintBase implements ContainerFactoryPluginInterface {

  /**
   * The route admin context to determine whether a route is an admin one.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Constructs constraint plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The route admin context.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AdminContext $admin_context) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->adminContext = $admin_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('router.admin_context')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return $this->t('Applies to all admin routes.');
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    return $this->adminContext->isAdminRoute();
  }

}
