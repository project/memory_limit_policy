<?php

namespace Drupal\memory_limit_policy_route\Plugin\MemoryLimitConstraint;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteProvider;
use Drupal\memory_limit_policy\MemoryLimitConstraintBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Configure the memory limit based on route.
 *
 * @MemoryLimitConstraint(
 *   id = "route",
 *   title = @Translation("Route"),
 *   description = @Translation("Provide a list of routes where the memory limit must be overridden.")
 * )
 */
class Route extends MemoryLimitConstraintBase implements ContainerFactoryPluginInterface {

  /**
   * Current route.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRoute;

  /**
   * The route admin context to determine whether a route is an admin one.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * The route provider service used to validate the filled routes.
   *
   * @var \Drupal\Core\Routing\RouteProvider
   */
  protected $routeProvider;

  /**
   * Constructs constraint plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route
   *   The current route.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The route admin context.
   * @param \Drupal\Core\Routing\RouteProvider $route_provider
   *   The route provider.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentRouteMatch $current_route, AdminContext $admin_context, RouteProvider $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentRoute = $current_route;
    $this->adminContext = $admin_context;
    $this->routeProvider = $route_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('router.admin_context'),
      $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['routes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Routes'),
      '#description' => $this->t('Enter one route per line.'),
      '#default_value' => implode(PHP_EOL, $this->getConfiguration()['routes'] ?? []) ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    // Get a clean array of routes from the textarea field.
    $routes = array_filter(
      preg_split("/\r?\n/", $form_state->getValue('routes')),
      function ($route) {
        return trim($route);
      }
    );

    // Loop through the routes to validate these are valid.
    foreach ($routes as $route) {
      try {
        $this->routeProvider->getRouteByName($route);
      }
      catch (RouteNotFoundException $e) {
        $form_state->setErrorByName('routes', $this->t('The route @route does not exist.', ['@route' => $route]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    // Transform textarea into list of values.
    $this->configuration['routes'] = array_filter(
      preg_split("/\r?\n/", $form_state->getValue('routes')),
      function ($route) {
        return trim($route);
      }
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return $this->t('Routes: @routes', ['@routes' => implode(', ', $this->getConfiguration()['routes'])]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $configured_route_list = $this->getConfiguration()['routes'] ?? [];
    if (in_array($this->currentRoute->getRouteName(), $configured_route_list)) {
      return TRUE;
    }

    return parent::evaluate();
  }

}
