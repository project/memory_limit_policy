<?php

namespace Drupal\memory_limit_policy\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;
use Drupal\memory_limit_policy\MemoryLimitPolicyInterface;

/**
 * Defines a Memory Limit Policy configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "memory_limit_policy",
 *   label = @Translation("Memory limit policy"),
 *   label_singular = @Translation("Memory limit policy"),
 *   label_plural = @Translation("Memory limit policies"),
 *   label_count = @PluralTranslation(
 *     singular = @Translation("memory limit policy"),
 *     plural = @Translation("memory limit policies"),
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\memory_limit_policy\Controller\MemoryLimitPolicyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\memory_limit_policy\Form\MemoryLimitPolicyFormAdd",
 *       "edit" = "Drupal\memory_limit_policy\Form\MemoryLimitPolicyFormEdit",
 *       "delete" = "Drupal\memory_limit_policy\Form\MemoryLimitPolicyDeleteForm",
 *       "enable" = "Drupal\memory_limit_policy\Form\MemoryLimitPolicyEnableForm",
 *       "disable" = "Drupal\memory_limit_policy\Form\MemoryLimitPolicyDisableForm",
 *     },
 *   },
 *   config_prefix = "memory_limit_policy",
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight",
 *     "memory" = "memory",
 *     "langcode" = "langcode",
 *     "policy_constraints" = "policy_constraints"
 *   },
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/performance/memory-limit-policy/add",
 *     "edit-form" = "/admin/config/performance/memory-limit-policy/{memory_limit_policy}",
 *     "delete-form" = "/admin/config/performance/memory-limit-policy/policy/{memory_limit_policy}/delete",
 *     "collection" = "/admin/config/performance/memory-limit-policy/list",
 *     "enable" = "/admin/config/performance/memory-limit-policy/{memory_limit_policy}/enable",
 *     "disable" = "/admin/config/performance/memory-limit-policy/{memory_limit_policy}/disable",
 *   }
 * )
 */
class MemoryLimitPolicy extends ConfigEntityBase implements MemoryLimitPolicyInterface {

  /**
   * The ID of the memory limit policy.
   *
   * @var int
   */
  protected $id;

  /**
   * The policy title.
   *
   * @var string
   */
  protected $label;

  /**
   * Constraint instance IDs.
   *
   * @var array
   */
  protected $policy_constraints = [];

  /**
   * The memory for this policy.
   *
   * @var int
   */
  protected $memory;

  /**
   * The weight for this policy.
   *
   * @var int
   */
  protected $weight;

  /**
   * The status for this policy.
   *
   * @var bool
   */
  protected $status;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * Return the constraints from the policy.
   *
   * @return array
   *   The policies constraints.
   */
  public function getConstraints() {
    return $this->policy_constraints;
  }

  /**
   * Return a specific constraint from the policy.
   *
   * @param int $key
   *   The constraint index in constraints list.
   *
   * @return \Drupal\memory_limit_policy\MemoryLimitConstraintInterface
   *   A specific constraint in the policy.
   */
  public function getConstraint($key) {
    if (!isset($this->policy_constraints[$key])) {
      return NULL;
    }
    return $this->policy_constraints[$key];
  }

  /**
   * Return the memory settings from the policy.
   *
   * @return int
   *   The memory to set.
   */
  public function getMemory() {
    return $this->memory;
  }

  /**
   * Evaluate the policy to check if it applies.
   *
   * @return bool
   *   TRUE if the policy applies, FALSE otherwise.
   */
  public function evaluate() {
    foreach ($this->getConstraints() as $constraint) {
      $plugin = \Drupal::service('plugin.manager.memory_limit_policy.memory_limit_constraint');

      /** @var \Drupal\memory_limit_policy\MemoryLimitConstraintInterface $constraint */
      $constraint = $plugin->createInstance($constraint['id'], $constraint);

      if ($constraint->isNegated() ? $constraint->evaluate() : !$constraint->evaluate()) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Get the plugin collections used by this entity.
   *
   * @return Drupal\Core\Plugin\DefaultLazyPluginCollection
   *   An array of plugin collections, keyed by the property name they use to
   *   store their configuration.
   */
  public function getConstraintsCollection() {
    if (!isset($this->constraintsCollection)) {
      $this->constraintsCollection = new DefaultLazyPluginCollection(\Drupal::service('plugin.manager.memory_limit_policy.memory_limit_constraint'), $this->getConstraints());
    }

    return $this->constraintsCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    $constraints_collection = $this->getConstraintsCollection();
    if (empty($constraints_collection)) {
      return $this;
    }

    $constraint_plugin_ids = $constraints_collection->getInstanceIds();
    foreach ($constraint_plugin_ids as $constraint_plugin_id) {
      $constraint_plugin = $constraints_collection->get($constraint_plugin_id);
      $constraint_plugin_dependencies = $this->getPluginDependencies($constraint_plugin);
      $this->addDependencies($constraint_plugin_dependencies);
    }

    return $this;
  }

}
