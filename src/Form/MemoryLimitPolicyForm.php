<?php

namespace Drupal\memory_limit_policy\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class MemoryLimitPolicyForm extends EntityForm {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Plugin manager for constraints.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $manager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructor.
   */
  public function __construct(PluginManagerInterface $manager, LanguageManagerInterface $language_manager, FormBuilderInterface $formBuilder) {
    $this->manager = $manager;
    $this->languageManager = $language_manager;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.memory_limit_policy.memory_limit_constraint'),
      $container->get('language_manager'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#open' => TRUE,
    ];

    $form['general']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Policy Name'),
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
      '#description' => $this->t('Enter label for this context.'),
    ];

    $form['general']['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine name'),
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'source' => ['general', 'label'],
        'exists' => [$this, 'memoryLimitPolicyExists'],
      ],
    ];

    $form['general']['memory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Memory'),
      '#default_value' => $this->entity->getMemory(),
      '#description' => $this->t(
        'The memory to set with this policy. Please add the unit too (see <a href="@url-doc">PHP documentation</a> for the expected directive format).',
        [
          '@url-doc' => Url::fromUri('https://www.php.net/manual/en/faq.using.php#faq.using.shorthandbytes')
            ->toString(),
        ]
      ),
      '#size' => 8,
    ];

    $form['general']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->id() ? $this->entity->status() : TRUE,
    ];

    return $form;
  }

  /**
   * Check to validate that the Memory Limit Policy name does not already exist.
   *
   * @param string $name
   *   The machine name of the context to validate.
   *
   * @return bool
   *   TRUE on context name already exist, FALSE on context name not exist.
   */
  public function memoryLimitPolicyExists($name) {
    $entity = $this->entityTypeManager->getStorage('memory_limit_policy')->loadByProperties(['name' => $name]);

    return (bool) $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if (!preg_match('#(^[\d]+[K|M|G]$)#', $form_state->getValue('memory'))) {
      $form_state->setError($form['general']['memory'], $this->t('The given memory is not valid.'));
    }
  }

}
