<?php

namespace Drupal\memory_limit_policy\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * The definition of the memory limit policy form.
 */
class MemoryLimitPolicyFormAdd extends MemoryLimitPolicyForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $formState) {
    $status = parent::save($form, $formState);

    if ($status) {
      $this->messenger()->addMessage($this->t('The memory limit policy %label has been added.', [
        '%label' => $this->entity->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The memory limit policy was not saved.'));
    }

    $formState->setRedirect('entity.memory_limit_policy.edit_form', [
      'memory_limit_policy' => $this->entity->id(),
    ]);
  }

}
