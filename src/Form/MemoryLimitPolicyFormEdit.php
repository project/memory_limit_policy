<?php

namespace Drupal\memory_limit_policy\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\memory_limit_policy\Entity\MemoryLimitPolicy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form that lists out the constraints for the policy.
 */
class MemoryLimitPolicyFormEdit extends MemoryLimitPolicyForm {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Plugin manager for constraints.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $manager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Overridden constructor to load the plugin.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $manager
   *   Plugin manager for constraints.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(PluginManagerInterface $manager, FormBuilderInterface $form_builder, MessengerInterface $messenger) {
    $this->manager = $manager;
    $this->formBuilder = $form_builder;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.memory_limit_policy.memory_limit_constraint'),
      $container->get('form_builder'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $constraints = [];
    foreach ($this->manager->getDefinitions() as $plugin_id => $definition) {
      $constraints[$plugin_id] = (string) $definition['title'];
    }
    asort($constraints);

    if (empty($constraints)) {
      $this->messenger->addWarning($this->t('No constraint available. Enable a module providing a constraint type.'));
    }
    else {
      $form['constraints_fieldset'] = [
        '#type' => 'fieldset',
        '#weight' => -10,
      ];
      $form['constraints_fieldset']['add_constraint_title'] = [
        '#markup' => '<h2>' . $this->t('Add Constraint') . '</h2>',
      ];

      $form['constraints_fieldset']['constraint'] = [
        '#type' => 'select',
        '#options' => $constraints,
        '#prefix' => '<table style="width=100%"><tr><td>',
        '#suffix' => '</td>',
      ];
      $form['constraints_fieldset']['add'] = [
        '#type' => 'submit',
        '#name' => 'add',
        '#value' => $this->t('Configure Constraint Settings'),
        '#ajax' => [
          'callback' => [$this, 'add'],
          'event' => 'click',
        ],
        '#prefix' => '<td>',
        '#suffix' => '</td></tr></table>',
      ];
    }

    $form['constraints_fieldset']['constraint_list'] = [
      '#markup' => '<h2>' . $this->t('Policy constraints') . '</h2>',
    ];

    $form['constraints_fieldset']['items'] = [
      '#type' => 'markup',
      '#prefix' => '<div id="configured-constraints">',
      '#suffix' => '</div>',
      '#theme' => 'table',
      '#header' => [
        'plugin' => $this->t('Plugin'),
        'summary' => $this->t('Summary'),
        'negated' => $this->t('Negated'),
        'operations' => $this->t('Operations'),
      ],
      '#rows' => $this->renderRows($this->entity),
      '#empty' => $this->t('No constraints have been configured. <b>The policy always applies.</b>'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    if ($status) {
      $this->messenger()->addMessage($this->t('The memory limit policy %label has been updated.', [
        '%label' => $this->entity->label(),
      ]));
      $form_state->setRedirect('entity.memory_limit_policy.collection');
    }
    else {
      $this->messenger()->addMessage($this->t('The memory limit policy was not saved.'));
    }
  }

  /**
   * Helper function to render the constraint rows for the policy.
   *
   * @param Drupal\memory_limit_policy\Entity\MemoryLimitPolicy $policy
   *   The policy to load the constraints from.
   *
   * @return array
   *   Constraint rows rendered for the policy.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function renderRows(MemoryLimitPolicy $policy) {
    $configured_conditions = [];

    foreach ($policy->getConstraints() as $row => $constraint) {
      /** @var \Drupal\memory_limit_policy\MemoryLimitConstraintInterface $instance */
      $instance = $this->manager->createInstance($constraint['id'], $constraint);

      $operations = $this->getOperations('entity.memory_limit_policy.constraint', [
        'memory_limit_policy_id' => $policy->id(),
        'machine_name' => $constraint['id'],
        'constraint_id' => $row,
      ]);

      $build = [
        '#type' => 'operations',
        '#links' => $operations,
      ];

      $configured_conditions[] = [
        'plugin' => Markup::create('<span title="' . $this->t('Plugin Id:') . ' ' . $instance->getPluginId() . '">' . $instance->getTitle() . '</span>'),
        'summary' => $instance->getSummary(),
        'negated' => $instance->isNegated() ? $this->t('Yes') : $this->t('No'),
        'operations' => [
          'data' => $build,
        ],
      ];
    }
    return $configured_conditions;
  }

  /**
   * Helper function to load edit operations for a constraint.
   *
   * @param string $route_name_base
   *   String representing the base of the route name for the constraints.
   * @param array $route_parameters
   *   Passing route parameter context to the helper function.
   *
   * @return array
   *   Set of operations associated with a constraint.
   */
  protected function getOperations($route_name_base, array $route_parameters = []) {
    $edit_url = new Url($route_name_base . '.edit', $route_parameters);
    $route_parameters['id'] = $route_parameters['constraint_id'];
    unset($route_parameters['constraint_id']);
    $delete_url = new Url($route_name_base . '.delete', $route_parameters);
    $operations = [];

    $operations['edit'] = [
      'title' => $this->t('Edit'),
      'url' => $edit_url,
      'weight' => 10,
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 700,
        ]),
      ],
    ];
    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'url' => $delete_url,
      'weight' => 100,
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 700,
        ]),
      ],
    ];
    return $operations;
  }

  /**
   * Ajax callback that manages adding a constraint.
   *
   * @param array $form
   *   Form definition of parent form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   State of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns the valid Ajax response from a modal window.
   */
  public function add(array &$form, FormStateInterface $form_state) {
    $constraint = $form_state->getValue('constraint');

    $content = $this->formBuilder->getForm(ConstraintEdit::class, $constraint, $form_state->getValue('id'));

    $content['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $url = Url::fromRoute('entity.memory_limit_policy.constraint.add', [
      'machine_name' => $this->entity->id(),
      'constraint_id' => $constraint,
    ], ['query' => [FormBuilderInterface::AJAX_FORM_REQUEST => TRUE]]);
    $content['submit']['#attached']['drupalSettings']['ajax'][$content['submit']['#id']]['url'] = $url->toString();

    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand($this->t('Configure required context'), $content, ['width' => '700']));

    return $response;
  }

}
